"use strict"

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  let root = document.getElementById('root')
  let list = document.createElement('ul');
     root.append(list);
//For...of method
  for(let {author,name,price} of books){
    try{

    if(author == undefined) throw `Book called "` +name + `" haven't info about author`
    if(name == undefined) throw "Book's name is not given"
    if(price == undefined) throw 'Price of book called "'+ name + '" is not given'

    const listItem = document.createElement('li');
    let bookName = document.createElement('h3');
    let bookAuthor = document.createElement('p');
    let bookPrice = document.createElement('p');
    bookName.innerText = name;
    bookAuthor.innerText = `Author: ${author}`;
    bookPrice.innerText = `Price: ${price}`;
    listItem.appendChild(bookName);
    listItem.appendChild(bookAuthor);
    listItem.appendChild(bookPrice);
    list.append(listItem);
    }
    catch(err){
        console.log(err)
    }

}


//forEach method

// books.forEach(book=>{
//     try{

//         if(book.author == undefined) throw `Book called "` +book.name + `" haven't info about author`
//         if(book.name == undefined) throw "Book's name is not given"
//         if(book.price == undefined) throw 'Price of book called "'+ book.name + '" is not given'
    
//         const listItem = document.createElement('li');
//         let bookName = document.createElement('h3');
//         let bookAuthor = document.createElement('p');
//         let bookPrice = document.createElement('p');
//         bookName.innerText = book.name;
//         bookAuthor.innerText = `Author: ${book.author}`;
//         bookPrice.innerText = `Price: ${book.price}`;
//         listItem.appendChild(bookName);
//         listItem.appendChild(bookAuthor);
//         listItem.appendChild(bookPrice);
//         list.append(listItem);
//     } catch(err){
//             console.log(err)
//         }
    
// })